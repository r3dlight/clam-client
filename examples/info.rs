use clam_client::client::ClamClient;

fn main() {
    let client = ClamClient::new("127.0.0.1", 3310).unwrap();
    println!("ClamD info:\n\t{:?}\n", client.version().unwrap());
}
