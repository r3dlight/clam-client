#![deny(missing_docs)]
#![forbid(unsafe_code)]
#![warn(unused_extern_crates)]
#![forbid(non_shorthand_field_patterns)]
#![warn(dead_code)]
#![warn(trivial_casts)]
#![warn(trivial_numeric_casts)]
#![warn(unstable_features)]
#![warn(unused_extern_crates)]
#![warn(unused_import_braces)]
#![warn(unused_qualifications)]
#![warn(variant_size_differences)]
#![forbid(private_in_public)]
#![warn(overflowing_literals)]
#![warn(deprecated)]

//! # clam_client - a client implementation for ClamAV written in Rust.
//! `clam_client`, provides a simple interface to all basic ClamAV functionality, currently
//! the only thing missing is sessions/multi threaded scanning, which may or may not be added
//! depending on demand.
//!
//! ## Example
//! ```rust
//! use clam_client::client::ClamClient;
//! use std::env;
//!
//! fn main() {
//!     if let Some(path) = env::args().nth(1) {
//!         let client = ClamClient::new("127.0.0.1", 3310).unwrap();
//!         println!(
//!             "Scan for '{}':\n\t{:?}\n",
//!             path,
//!             client.scan_path(&path, true).unwrap()
//!         );
//!     } else {
//!         println!("USAGE: cargo run --example simple \"<file_path>\"");
//!     }
//! }
//! ```
//!

#[cfg(feature = "serde")]
#[macro_use]
extern crate serde;


pub mod client;
pub mod error;
pub mod response;
